#!/bin/bash
cd projects/jwt-security-app/ || exit
echo "start pull"
git pull
echo "kill java -jar"
pkill -f 'java -jar'
echo "start deploy"
nohup java -jar ./build/libs/jwt-security-0.0.1-SNAPSHOT.jar </dev/null >command.log 2>&1 &
sleep 5s
echo "end deploy"
exit