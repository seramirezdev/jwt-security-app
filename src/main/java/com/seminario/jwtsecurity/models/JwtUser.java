package com.seminario.jwtsecurity.models;

import io.swagger.annotations.ApiModelProperty;

public class JwtUser {

    @ApiModelProperty(notes = "name of the User")
    private String username;
    @ApiModelProperty(notes = "Id of the User")
    private long id;
    @ApiModelProperty(notes = "Role of the User")
    private String role;

    public JwtUser() {

    }

    public JwtUser(String username, long id, String role) {
        this.username = username;
        this.id = id;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
