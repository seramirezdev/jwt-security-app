package com.seminario.jwtsecurity.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    List<VendorExtension<String>> vendors;

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.seminario.jwtsecurity"))
                .paths(regex("/.*"))
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo() {

        ApiInfo apiInfo = new ApiInfo(
                "Spring Boot Swagger Api with JWT",
                "Spring Boot Swagger Api with JWT for Seminario Class",
                "1.0",
                "Terms of Service",
                new Contact("Sergio Ramírez", "https://www.linkedin.com/in/seramirezdev/", "seramirezdev@gmail.com"),
                "Apache License Version 2.0",
                "https://www.apache.org/license.html",
                Arrays.asList()
        );

        return apiInfo;
    }
}
