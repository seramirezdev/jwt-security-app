package com.seminario.jwtsecurity.resources;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/hello")
@Api(value = "HelloWorld Resource")
public class HelloResource {

    @ApiOperation(value = "Return Hello World")
    @ApiResponses(value = {
            @ApiResponse(code = 100, message = "100 is the message"),
            @ApiResponse(code = 200, message = "Successful Helloworld")
    })
    @GetMapping("/hello")
    public String hello() {
        return "Hello World";
    }

    @ApiOperation(value = "Return param")
    @PostMapping("/add")
    public String add(@RequestBody final String hello) {
        return hello;
    }

    @ApiOperation(value = "Return param")
    @PutMapping("/put")
    public String put(@RequestBody final String hello) {
        return hello;
    }
}
