package com.seminario.jwtsecurity.resources;

import com.seminario.jwtsecurity.models.JwtUser;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/rest/user")
@Api(value = "User Resource REST Endpoint")
public class UserResource {

    @GetMapping
    public List<JwtUser> getUsers() {
        return Arrays.asList(
                new JwtUser("Sergio", 2000L, "admin"),
                new JwtUser("Andrés", 1000L, "client")
        );
    }

    @GetMapping("/{usernName}")
    public JwtUser getUser(@PathVariable("userName") final String userName) {
        return new JwtUser(userName, 2000L, "client");
    }
}
